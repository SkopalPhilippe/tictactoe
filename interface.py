import game
from tkinter import *

def selectNbPlayer():

    config = {}

    fenetre = Tk()
    fenetre.geometry("155x70") #You want the size of the app to be 500x500
    fenetre.resizable(0, 0) #Don't allow resizing in the x or y direction
    
    l = LabelFrame(fenetre, text="Choisir nombre de joueur")
    l.pack(fill="both", expand="yes")

    btn = Button(l, text='1', borderwidth=1, command=lambda: selectDifficulte(config, fenetre, 1))
    btn.grid(row=1, column=1)
    btn.config( height = 1, width = 20 )

    btn = Button(l, text='2', borderwidth=1, command=lambda: selectDifficulte(config, fenetre, 2))
    btn.grid(row=2, column=1)
    btn.config( height = 1, width = 20 )
    
    fenetre.mainloop()

def selectDifficulte(config, fenetre, previous):
    
    config["nbPlayer"] = previous

    if(previous == 1):

        fenetre.destroy()

        fenetre = Tk()
        fenetre.geometry("175x90") #You want the size of the app to be 500x500
        fenetre.resizable(0, 0) #Don't allow resizing in the x or y direction

        l = LabelFrame(fenetre, text="Choisir un niveau de difficulté")
        l.pack(fill="both", expand="yes")

        btn = Button(l, text='Facile', borderwidth=1, command=lambda: play(config, fenetre, "Facile"))
        btn.grid(row=1, column=1)
        btn.config( height = 1, width = 23 )

        btn = Button(l, text='Moyen', borderwidth=1, command=lambda: play(config, fenetre, "Moyen"))
        btn.grid(row=2, column=1)
        btn.config( height = 1, width = 23 )

        btn = Button(l, text='Difficile', borderwidth=1, command=lambda: play(config, fenetre, "Difficile"))
        btn.grid(row=3, column=1)
        btn.config( height = 1, width = 23 )

        fenetre.mainloop()
    else:
        play(config, fenetre, "")

def play(config, fenetre, previous):

    fenetre.destroy()

    config["difficulte"] = previous
    game.morpion(fenetre, config)
    

def displayGrid(fenetre, config):

    fenetre = Tk()
    fenetre.geometry("155x180") #You want the size of the app to be 500x500
    fenetre.resizable(0, 0) #Don't allow resizing in the x or y direction

    txtAquiLeTour = "C'est le tour du joueur: " + str(config["whoPlay"])

    lbl = LabelFrame(fenetre, text=txtAquiLeTour)
    lbl.pack(fill="both", expand="yes")

    l = -1
    for i in config["selected"]:
        l += 1
        k = -1
        for j in i:
            k += 1
            if i[k] == 1:
                addCross(lbl, k, l)
            elif i[k] == 2:
                addCircle(lbl, k, l)
            else:
                addSelectable(lbl, k, l, fenetre, config)

    fenetre.mainloop()

def addCross(lbl, posX, posY):
    btn = Button(lbl, text='X', borderwidth=1)
    btn.grid(row=posY, column=posX)
    btn.config( height = 3, width = 6 )

def addCircle(lbl, posX, posY):
    btn = Button(lbl, text='O', borderwidth=1)
    btn.grid(row=posY, column=posX)
    btn.config( height = 3, width = 6 )

def addSelectable(lbl, posX, posY, fenetre,  config):
    btn = Button(lbl, borderwidth=1, command= lambda: game.select(fenetre, config,  posY, posX))
    btn.grid(row=posY, column=posX)
    btn.config( height = 3, width = 6 )
        
def displayResult(config):
    
    fenetre = Tk()
    fenetre.geometry("155x110") #You want the size of the app to be 500x500
    fenetre.resizable(0, 0) #Don't allow resizing in the x or y direction

    l = LabelFrame(fenetre, text="Affichage du vainqueur")
    l.pack(fill="both", expand="yes")

    if config["winner"] == 1 or config["winner"] == 2:
        txt = "Le joueur : " + str(config["winner"])
    elif config["winner"] == -1:
        txt = "Egalité, pas de vainqueur"
    
    Label(l, text=txt).grid(row=1, column=1)

    Label(l, text="Re-jouer ?").grid(row=3, column=1)

    btn = Button(l, text='OUI', borderwidth=1, command=lambda: game.reLaunch(fenetre))
    btn.grid(row=4, column=1)
    btn.config( height = 1, width = 20 )

    btn = Button(l, text='NON', borderwidth=1, command=lambda: fenetre.destroy())
    btn.grid(row=5, column=1)
    btn.config( height = 1, width = 20)

    fenetre.mainloop()