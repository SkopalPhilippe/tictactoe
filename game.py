import ia
import interface
import random

def launch():
    interface.selectNbPlayer()

def reLaunch(fenetre):
    fenetre.destroy()
    interface.selectNbPlayer()

def isWon(config, curPlayer):
    
    tab = config["selected"]

    # 111 - 000 - 000
    if tab[0][0] == curPlayer and  tab[0][1] == curPlayer and tab[0][2] == curPlayer:
        return True
    # 000 - 111 - 000
    elif  tab[1][0] == curPlayer and  tab[1][1] == curPlayer and tab[1][2] == curPlayer:
        return True
    # 000 - 000 - 111
    elif tab[2][0] == curPlayer and  tab[2][1] == curPlayer and tab[2][2] == curPlayer:
        return True


    # 100 - 010 - 001
    elif tab[0][0] == curPlayer and  tab[1][1] == curPlayer and tab[2][2] == curPlayer:
        return True
    # 001 - 010 - 100
    elif tab[2][0] == curPlayer and  tab[1][1] == curPlayer and tab[0][2] == curPlayer:
        return True

    # 100 - 100 - 100
    elif tab[0][0] == curPlayer and  tab[1][0] == curPlayer and tab[2][0] == curPlayer:
        return True
    # 010 - 010 - 010
    elif tab[0][1] == curPlayer and  tab[1][1] == curPlayer and tab[2][1] == curPlayer:
        return True
    # 001 - 001 - 001
    elif tab[0][2] == curPlayer and  tab[1][2] == curPlayer and tab[2][2] == curPlayer:
        return True

    else:
        return False
    
def isDraw(config):

    for i in config["selected"] :
        for j in i:
            if j == 0:
                return False
    return True

def isFinished(config):

    if isWon(config, 1):
        config["winner"] = 1
    elif isWon(config, 2):
        config["winner"] = 2
    elif isDraw(config):
        config["winner"] = -1

def select(fenetre, config, i, j):

    # sélection
    config["selected"][i][j] = config["whoPlay"]

    # interversion des joueurs
    if config["whoPlay"] == 1:
        config["whoPlay"] = 2
    elif config["whoPlay"] == 2:
        config["whoPlay"] = 1

    # ferme la fenêtre actuelle
    fenetre.destroy()

    # vérifie si un joueur gagne, -1 = égalité
    isFinished(config)

    if config["winner"] == 1:
        interface.displayResult(config)
    elif config["winner"] == 2:
        interface.displayResult(config)
    elif config["winner"] == -1:
        interface.displayResult(config)
    else: 
        # check if ia is in the game
        if config["nbPlayer"] == 1:
            ia_check(fenetre, config)
        else:
            interface.displayGrid(fenetre, config)
        
def whoStart():
    return random.randint(1, 2)

def morpion(fenetre, config):

    config["winner"] = 0
    config["whoPlay"] = whoStart()
    config["selected"] = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    # check if ia is in the game
    if config["nbPlayer"] == 1:
        ia_check(fenetre, config)
    else:
        interface.displayGrid(fenetre, config)

def ia_check(fenetre, config):

    # check if ia has to play
    if config["whoPlay"] == 2:

        # get next move
        ia.select(config)
        config["whoPlay"] = 1

        # check again if win
        isFinished(config)

        if config["winner"] == 1:
            interface.displayResult(config)
        elif config["winner"] == 2:
            interface.displayResult(config)
        elif config["winner"] == -1:
            interface.displayResult(config)
        else: 
            interface.displayGrid(fenetre, config)