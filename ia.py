import random

def select(config):
    
    # sélection bancale ...
    if config["difficulte"] == "Facile":
        easy_ia(config)
    elif config["difficulte"] == "Moyen":      
        easy_ia(config)
    elif config["difficulte"] == "Difficile":        
        easy_ia(config)

def easy_ia(config):

    flag = True
    x = -1
    y = -1

    while flag:

        x = random.randint(0, 2)
        y = random.randint(0, 2)

        print(config["selected"][x][y])

        if config["selected"][x][y] == 0:
            flag = False
            config["selected"][x][y] = 2